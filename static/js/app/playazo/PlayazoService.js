playazoModule.service('playazoService', ['$q', '$http', function($q, $http) {

    this.ACrearPlaya = function(crearPlayaForm) {
        return  $http({
          url: "playazo/ACrearPlaya",
          data: crearPlayaForm,
          method: 'POST',
        });
    //    var labels = ["/VPlaya", "/VCrearPlaya", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VPlayas = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'playazo/VPlayas',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AModifPlaya = function(modifPlayaForm) {
        return  $http({
          url: "playazo/AModifPlaya",
          data: modifPlayaForm,
          method: 'POST',
        });
    //    var labels = ["/VPlaya", "/VModifPlaya", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VCrearPlaya = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'playazo/VCrearPlaya',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.ARevertirCambio = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'playazo/ARevertirCambio',
          data: args,
          method: 'POST'
        });
    //    var labels = ["/VPlaya", "/VCambio", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
    this.VModifPlaya = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'playazo/VModifPlaya',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VPlaya = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'playazo/VPlaya',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VCambio = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'playazo/VCambio',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

}]);