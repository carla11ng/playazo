playazoModule.config(function ($routeProvider) {
    $routeProvider.when('/VPlayas', {
                controller: 'VPlayasController',
                templateUrl: 'app/playazo/VPlayas.html'
            }).when('/VCrearPlaya', {
                controller: 'VCrearPlayaController',
                templateUrl: 'app/playazo/VCrearPlaya.html'
            }).when('/VModifPlaya/:idPlaya', {
                controller: 'VModifPlayaController',
                templateUrl: 'app/playazo/VModifPlaya.html'
            }).when('/VPlaya/:idPlaya', {
                controller: 'VPlayaController',
                templateUrl: 'app/playazo/VPlaya.html'
            }).when('/VCambio/:idPlaya', {
                controller: 'VCambioController',
                templateUrl: 'app/playazo/VCambio.html'
            });
});

playazoModule.controller('VPlayasController', 
   ['$scope', '$location', '$route', 'flash', 'playazoService',
    function ($scope, $location, $route, flash, playazoService) {
      $scope.msg = '';
      playazoService.VPlayas().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.VPlaya0 = function(idPlaya) {
        $location.path('/VPlaya/'+idPlaya);
      };
      $scope.VCrearPlaya1 = function() {
        $location.path('/VCrearPlaya');
      };

    }]);
playazoModule.controller('VCrearPlayaController', 
   ['$scope', '$location', '$route', 'flash', 'playazoService',
    function ($scope, $location, $route, flash, playazoService) {
      $scope.msg = '';
      $scope.crearPlayaForm = {};

      playazoService.VCrearPlaya().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.VPlayas1 = function() {
        $location.path('/VPlayas');
      };

      $scope.crearPlayaFormSubmitted = false;
      $scope.ACrearPlaya0 = function(isValid) {
        $scope.crearPlayaFormSubmitted = true;
        if (isValid) {
          
          playazoService.ACrearPlaya($scope.crearPlayaForm).then(function (object) {
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              $location.path(label);
              $route.reload();
          });
        }
      };

    }]);
playazoModule.controller('VModifPlayaController', 
   ['$scope', '$location', '$route', 'flash', '$routeParams', 'playazoService',
    function ($scope, $location, $route, flash, $routeParams, playazoService) {
      $scope.msg = '';
      $scope.modifPlayaForm = {};

      playazoService.VModifPlaya({"idPlaya":$routeParams.idPlaya}).then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        $scope.modifPlayaForm.nombre = $scope.playa.nombre;
        $scope.modifPlayaForm.ubicacion = $scope.playa.ubicacion;
        $scope.modifPlayaForm.descripcion = $scope.playa.descripcion;
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.VPlaya1 = function(idPlaya) {
        $location.path('/VPlaya/'+idPlaya);
      };

      $scope.modifPlayaFormSubmitted = false;
      $scope.AModifPlaya0 = function(isValid) {
        $scope.modifPlayaFormSubmitted = true;
        if (isValid) {
          $scope.modifPlayaForm.idPlaya = $scope.playa.id;
          playazoService.AModifPlaya($scope.modifPlayaForm).then(function (object) {
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              $location.path(label);
              $route.reload();
          });
        }
      };

    }]);
playazoModule.controller('VPlayaController', 
   ['$scope', '$location', '$route', 'flash', '$routeParams', 'playazoService',
    function ($scope, $location, $route, flash, $routeParams, playazoService) {
      $scope.msg = '';
      playazoService.VPlaya({"idPlaya":$routeParams.idPlaya}).then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.VModifPlaya0 = function(idPlaya) {
        $location.path('/VModifPlaya/'+idPlaya);
      };
      $scope.VCambio1 = function(idPlaya) {
        $location.path('/VCambio/'+idPlaya);
      };
      $scope.VPlayas2 = function() {
        $location.path('/VPlayas');
      };

    }]);
playazoModule.controller('VCambioController', 
   ['$scope', '$location', '$route', 'flash', '$routeParams', 'playazoService',
    function ($scope, $location, $route, flash, $routeParams, playazoService) {
      $scope.msg = '';
      playazoService.VCambio({"idPlaya":$routeParams.idPlaya}).then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.ARevertirCambio0 = function() {
          
        playazoService.ARevertirCambio({"idPlaya":$routeParams.idPlaya}).then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          $location.path(label);
          $route.reload();
        });};
      $scope.VPlaya1 = function(idPlaya) {
        $location.path('/VPlaya/'+idPlaya);
      };

    }]);
