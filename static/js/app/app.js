// Creación del módulo de la aplicación
var playazoModule = angular.module('playazo', ['ngRoute', 'ngAnimate', 'flash']);
playazoModule.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
                controller: 'VPlayasController',
                templateUrl: 'app/playazo/VPlayas.html'
            });
});
playazoModule.controller('playazoController_',  ['$scope', '$http', '$location',
function($scope) {
    $scope.title = "DisenoPlayazoV2";
}]);
