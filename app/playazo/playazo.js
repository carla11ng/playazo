module.exports = function (app) {
    var module = {};

var uri = 'mongodb://usuario:usuario@ds043002.mongolab.com:43002/playazo';
var mongoClient=require('mongodb').MongoClient;
var objectID = require('mongodb').ObjectID;
var objetoDB;

app.post('/playazo/ACrearPlaya', function(request, response) {
    //POST/PUT parameters
    params = request.body;
    results = [{'label':'/VPlaya', 'msg':['Playa registrada']}, {'label':'/VCrearPlaya', 'msg':['Playa no registrada']}, ];
    res = results[0];
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    mongoClient.connect(uri, function(err, db) {
        if (err) {
            console.log(err);
            res = results[1];
        }
        else {
            objetoDB = { db: db, playas: db.collection('playas') };
            
            var playa = params;
            playa['historial'] = [];
            playa['fecha'] = (new Date()).getTime();
            
            objetoDB.playas.insert(playa, {w: 1}, function(err, result) {
                if (err) {
                    console.log(err);
                    res = results[1];
                }
                else {
                    res.label += '/' + result.ops[0]._id;
                    db.close();
                    $next();
            }});
    }});
    //Action code ends here
    })(function(){response.json(res);});

});
app.post('/playazo/AModifPlaya', function(request, response) {
    //POST/PUT parameters
    params = request.body;
    results = [{'label':'/VPlaya/'+params.idPlaya, 'msg':['Playa modificada']}, {'label':'/VModifPlaya', 'msg':['Playa no modificada']}, ];
    res = results[0];
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    mongoClient.connect(uri, function(err, db) {
        if (err)  {
            console.log(err);
            res = results[1];
        }
        else {
            objetoDB = {db: db, playas: db.collection('playas')};
            var id = new objectID(params.idPlaya);
            
            objetoDB.playas.findOne({_id: id}, function(err, doc) {
                if (err) {
                    console.log(err);
                    res = results[1];
                }
                else {
                    var fech = (new Date()).getTime();
                    var hist = doc['historial'];
                    
                    hist.unshift({'nombre': doc.nombre,
                        'ubicacion': doc.ubicacion,
                        'descripcion': doc.descripcion,
                        'fecha': doc.fecha
                    });
                    objetoDB.playas.update({_id: id},
                        {$set:
                            {   nombre: params.nombre,
                                ubicacion: params.ubicacion,
                                descripcion: params.descripcion,
                                historial: hist,
                                fecha: fech}},
                        {w: 1},
                        function(err) {
                            if (err)  {
                                console.log(err);
                                res = results[1];
                            }
                            else {
                                db.close();
                                $next();
                            }});
                }});
            //db.close() ???
    }});
    //Action code ends here
    })(function(){response.json(res);});

});
app.post('/playazo/ARevertirCambio', function(request, response) {
    //POST/PUT parameters
    params = request.body;
    results = [{'label':'/VPlaya/'+ params.idPlaya, 'msg':['Cambio revertido']}, {'label':'/VCambio', 'msg':['Cambio no revertido']}, ];
    res = results[0];
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    mongoClient.connect(uri, function(err, db) {
        if (err) {
            console.log(err);
            res = results[1];
        }
        else {
            objetoDB = {db: db, playas: db.collection('playas')};
            var id = new objectID(params.idPlaya);
            objetoDB.playas.findOne({_id: id}, function(err, doc) {
                if (err) {
                    console.log(err);
                    res = results[1];
                }
                else {
                    var hist = doc.historial;
                    var ultm = hist.shift();
                    objetoDB.playas.update({_id: id},
                        {$set:
                                { nombre: ultm.nombre,
                                  ubicacion: ultm.ubicacion,
                                  descripcion: ultm.descripcion,
                                  historial: hist,
                                  fecha: ultm.fecha}},
                        {w: 1},
                        function(err) {
                            if (err) {
                                console.log(err);
                                res = results[1];
                            }
                            else {
                                db.close();
                                $next();
                        }});
                }});
        }});
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/playazo/VCambio', function(request, response) {
    //GET parameter
    idPlaya = request.query['idPlaya'];
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    mongoClient.connect(uri, function(err, db) {
        if (err) {
            console.log(err);
        }
        else {
            objetoDB = {db: db, playas: db.collection('playas')};
            var id = new objectID(idPlaya);
            objetoDB.playas.findOne({_id: id}, function(err, doc) {
                if (err)  {
                   console.log(err);
                }
                else {
                    res.playa = {'id': doc._id,
                        'nombre': doc.nombre,
                        'ubicacion': doc.ubicacion,
                        'descripcion': doc.descripcion,
                        'fecha': (new Date(doc.fecha)).toLocaleString()
                    };
                    res.historial = [];

                    var histLength = doc.historial.length;
                    for (var k = 0; k < histLength; k++) {
                        res.historial[k] = {
                            nombre: doc.historial[k].nombre,
                            ubicacion: doc.historial[k].ubicacion,
                            descripcion: doc.historial[k].descripcion,
                            fecha: (new Date(doc.historial[k].fecha)).toLocaleString()
                        };
                    }
                    db.close();
                    $next();
                }
            });
            //db.close();
        }
    });
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/playazo/VCrearPlaya', function(request, response) {
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/playazo/VModifPlaya', function(request, response) {
    //GET parameter
    idPlaya = request.query['idPlaya'];
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    mongoClient.connect(uri, function(err, db) {
        if (err) {
            console.log(err);
        }
        else {
            objetoDB = {db: db, playas: db.collection('playas')};
            var id = new objectID(idPlaya);
            var cursor = objetoDB.playas.findOne({_id: id}, function (err, doc) {
                if (err) {
                    console.log(err);
                }
                else {
                    res.playa = {'id': doc._id,
                        'nombre': doc.nombre,
                        'ubicacion': doc.ubicacion,
                        'descripcion': doc.descripcion,
                        'fecha': (new Date(doc.fecha)).toLocaleString()
                    };
                    db.close();
                    $next();
            }});
    }});
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/playazo/VPlaya', function(request, response) {
    //GET parameter
    idPlaya = request.query['idPlaya'];
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    mongoClient.connect(uri, function(err, db) {
        objetoDB = {db: db, playas: db.collection('playas')};
        var id = new objectID(idPlaya);
        objetoDB.playas.findOne({_id: id}, function (err, doc) {
            if (err) {
                console.log(err);
            }
            else {
                res.playa = {'id': doc._id,
                    'nombre': doc.nombre,
                    'ubicacion': doc.ubicacion,
                    'descripcion': doc.descripcion,
                    'fecha': (new Date(doc.fecha)).toLocaleString()
                };
                db.close();
                $next();
        }});
    });
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/playazo/VPlayas', function(request, response) {
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    res.playas = [];
    mongoClient.connect(uri, function(err, db) {
        if (err) {
            console.log(err);
        }
        else {
            objetoDB = { db: db, playas: db.collection('playas') };

            var cursor = objetoDB.playas.find();
            cursor.toArray(function(err, docs) {
                if (err) {
                    console.log(err);
                }
                else {
                    var arrayLength = docs.length;
                    for (var k = 0; k < arrayLength; k++) {
                        res.playas.push(
                            {id: docs[k]._id,
                             nombre: docs[k].nombre,
                             ubicacion: docs[k].ubicacion,
                             descripcion: docs[k].descripcion,
                             fecha: docs[k].fecha.toLocaleString()
                        });
                    }
                }
                db.close();
                $next();
            });
    }});

    //Action code ends here
    })(function(){response.json(res);});

});

//Use case code starts here


//Use case code ends here

    return module;

}

