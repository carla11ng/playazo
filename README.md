# Playazo

Para correr esta aplicación debe tener instalados los comandos node y npm.

Paquetes necesarios express, express-session y body-parser:

        $ npm install express           # (Probablemente ya instalado)
        $ npm install express-session
        $ npm install body-parser

Para ejecutar el servidor:

        $ node app.js

